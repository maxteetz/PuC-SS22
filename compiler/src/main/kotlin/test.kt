import highlighting.colorSettings
import kotlinx.collections.immutable.persistentHashMapOf
import substitution.Color

fun testLex(input: String) {
    val lexer = Lexer(input)
    do {
        println(lexer.next())
    } while (lexer.lookahead() != Token.EOF)
}

fun testParse(input: String) {
    val parser = Parser(Lexer(input))
    val expr = parser.parseExpression()
    print(expr)
}

fun test(input: String) {
    val parser = Parser(Lexer(input))
    val expr = parser.parseExpression()
    print(
        eval(
            persistentHashMapOf(), expr
        )
    )
}

/*
fun main() {
//  testLex("""-> => == / * + -""")
    testLex(
        """
      "Hello"
   """.trimMargin()
    )
}
*/
//-------------------------------------------------------------------------//
val initialEnv: Env = persistentHashMapOf(
    "firstChar" to Value.Closure(
        emptyEnv, "x",
        Expr.Var("#firstChar")
    ),
    "remainingChars" to Value.Closure(
        emptyEnv, "x",
        Expr.Var("#remainingChars")
    ),
    "charCode" to Value.Closure(
        emptyEnv, "x",
        Expr.Var("#charCode")
    ),
    "codeChar" to Value.Closure(
        emptyEnv, "x",
        Expr.Var("#codeChar")
    )
)

fun printEvaledAndType(input: String) {
    val expr = Parser(Lexer(input)).parseExpression()
    val ty = infer(initialContext, expr)

    println("${eval(initialEnv, expr)} : ${prettyPoly(generalize(initialContext, applySolution(ty)))}")
}

fun printExpression(input: String) {
    val expr = Parser(Lexer(input)).parseExpression()
    // val ty = infer(initialContext, expr)
    //println("${eval(initialEnv, expr)}")
    println(expr)
}

/*fun main() {
    testInput("""
    let hello = "Hello" in
    let world = "World" in
    let join = \s1 => \s2 => s1 # " " # s2 in
    let shout = \s => s # "!" in
    let twice = \f => \x => f (f x) in
    twice (twice shout) (join hello world)
  """.trimIndent())
}*/

/* """
    let hello = "Hello" in
    let world = "World" in
    let join = \s1 => \s2 => s1 # " " # s2 in
    join hello world
  """*/

fun main() {

    val colors = mapOf<String, Color>(
        "binderLambda" to Color.BLUE,
        "binderLet" to Color.RED,
        "var" to Color.YELLOW,
        "let" to Color.RESET,
        "operator" to Color.RESET,
        "rec" to Color.RESET,
        "int" to Color.RESET,
        "bool" to Color.RESET,
        "string" to Color.RESET,
        "if" to Color.RESET
    )
    colorSettings.setColors(colors)

    val exprString =
        /*
            let hello = "Hello" in
            let world = "World" in
            let join = \s1 => \s2 => s1 # " " # s2 in
            if join hello world == "Hello World" then 1 else 2
        */

        """
    let hello = "Hello" in
    let world = "World" in
    let join = \s1 => \s2 => s1 # " " # s2 in
    let shout = \s => s # "!" in
    let twice = \f => \x => f (f x) in
    if twice (twice shout) (join hello world) == "Hello World!!!!" then true else false
        """.trimIndent()

    val expr = Parser(Lexer(exprString)).parseExpression()

    printEvaledAndType(exprString)

    println(toColorString(expr) + "\n---------------------------------------------------")
    colorSettings.mode = colorSettings.modes.NONE
    println(toColorString(expr) + "\n---------------------------------------------------")
    colorSettings.mode = colorSettings.modes.FUN
    println(toColorString(expr) + "\n---------------------------------------------------")
    colorSettings.mode = colorSettings.modes.NORMAL
    println(toColorString(expr) + "\n---------------------------------------------------")
    colorSettings.mode = colorSettings.modes.CASCADING
    println(toColorString(expr))
}


