package highlighting

import substitution.Color

object colorSettings {

    enum class modes {
        NORMAL, FUN, NONE, CASCADING
    }

    var mode = modes.NORMAL
        set(value) {
            when (value) {
                modes.NORMAL ->
                    setToStandardSemanticColors()
                modes.FUN ->
                    setToFunColors()
                modes.CASCADING ->
                    setToCascadingColors()
                else ->
                    setToNoColors()
            }
            field = value
        }

    //var, binderLambda, binderLet, let, operator, rec, int, bool, string, if
    val possibleUnitsToColor =
        listOf("binderLambda", "binderLet", "var", "let", "operator", "rec", "int", "bool", "string", "if")

    var colorAllocations: Map<String, Color> =
        mapOf("binderLambda" to Color.BLUE, "binderLet" to Color.RED, "var" to Color.YELLOW)

    fun setColors(newColorAllocations: Map<String, Color>) {
        if (!newColorAllocations.toList().fold(true) { acc, i ->
                acc && possibleUnitsToColor.contains(i.first)
            }
        ) {
            println("Konnte Farbe nicht ändern")
            return
        }
        colorAllocations = newColorAllocations
    }


    private fun setToStandardSemanticColors() {
        colorAllocations = possibleUnitsToColor
            .filter { it.equals("binderLambda") || it.equals("binderLet") || it.equals("var") }
            .map {
                if (it.equals("binderLambda"))
                    it to Color.BLUE
                else if (it.equals("binderLet"))
                    it to Color.RED
                else
                    it to Color.YELLOW
            }.toMap()
    }

    private fun setToFunColors() {
        colorAllocations = possibleUnitsToColor
            .map {
                it to Color.randomColor()
            }.toMap()
    }

    private fun setToNoColors() {
        colorAllocations = mapOf()
    }

    private fun setToCascadingColors() {
        colorAllocations = mapOf(
            "0" to Color.BLUE,
            "1" to Color.GREEN,
            "2" to Color.YELLOW,
            "3" to Color.MAGENTA,
            "4" to Color.CYAN,
            "5" to Color.RED,
            "6" to Color.BLACK,
            "7" to Color.WHITE,
            "8" to Color.YELLOW_BRIGHT
        )
    }

    fun literalWithColor(string: String, color: Color): String {
        return "$color$string${Color.RESET}"
    }

    fun getColorOfUnit(unit: String) = colorAllocations[unit]

}