import highlighting.colorSettings
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentHashMapOf
import substitution.Color

//abc

sealed class Expr {
    data class Var(val name: String) : Expr()
    data class Lambda(val binder: String, val tyBinder: MonoType?, val body: Expr) : Expr()
    data class App(val func: Expr, val arg: Expr) : Expr()
    data class If(val condition: Expr, val thenBranch: Expr, val elseBranch: Expr) : Expr()
    data class Binary(val left: Expr, val op: Operator, val right: Expr) : Expr()
    data class Let(val recursive: Boolean, val binder: String, val expr: Expr, val body: Expr) : Expr()

    data class IntLiteral(val num: Int) : Expr()
    data class BoolLiteral(val bool: Boolean) : Expr()
    data class StringLiteral(val string: String) : Expr()
}




// exp.: params : s1, binderLambda
// for binder in let would be binderLet
fun toColorString(expr: Expr, params: MutableMap<String, String> = mutableMapOf()): String {
    return when (expr) {
        is Expr.IntLiteral -> {
            val color = colorSettings.getColorOfUnit("int")
            if (color != null) colorSettings.literalWithColor(expr.num.toString(), color)
            else expr.num.toString()
        }
        is Expr.BoolLiteral -> {
            val color = colorSettings.getColorOfUnit("bool")
            if (color != null) colorSettings.literalWithColor(expr.bool.toString(), color)
            else expr.bool.toString()
        }
        is Expr.StringLiteral -> {
            val color = colorSettings.getColorOfUnit("string")
            if (color != null) colorSettings.literalWithColor("\"${expr.string}\"", color)
            else "\"${expr.string}\""
        }
        is Expr.Binary -> {
            val left = toColorString(expr.left, params)
            val right = toColorString(expr.right, params)
            var arithmeticSign = expr.op.toArithmeticSignString()
            val color = colorSettings.getColorOfUnit("operator")
            if (color != null)
                arithmeticSign = colorSettings.literalWithColor(expr.op.toArithmeticSignString(), color)
            "$left $arithmeticSign $right"
        }
        is Expr.If -> {
            val colorCondition = toColorString(expr.condition, params)
            val colorThen = toColorString(expr.thenBranch, params)
            val colorElse = toColorString(expr.elseBranch, params)
            var ifComponentList = listOf("if", "then", "else")
            val color = colorSettings.getColorOfUnit("if")
            if (color != null)
                ifComponentList = ifComponentList.map { colorSettings.literalWithColor(it, color) }
            "${ifComponentList[0]} $colorCondition ${ifComponentList[1]} $colorThen ${ifComponentList[2]} $colorElse"
        }
        is Expr.Let -> {
            if(colorSettings.mode == colorSettings.modes.CASCADING)
                params[expr.binder] = params.size.toString()
            else
                params[expr.binder] = "binderLet"

            val coloredExpr = toColorString(expr.expr, params)
            val coloredBody = toColorString(expr.body, params)

            var ifComponentList = listOf("let", "rec", expr.binder)
            ifComponentList =
                ifComponentList.map {
                    // theoretisch binder - also binder farbe- aber hier eher var
                    if (it == expr.binder && colorSettings.getColorOfUnit("binderLet") != null
                        && colorSettings.mode != colorSettings.modes.CASCADING) {
                        colorSettings.literalWithColor(it, colorSettings.getColorOfUnit("binderLet")!!) + " "
                    }
                    else if(it == expr.binder && colorSettings.getColorOfUnit(params[expr.binder].toString()) != null
                        && colorSettings.mode == colorSettings.modes.CASCADING) {
                        colorSettings.literalWithColor(it, colorSettings.getColorOfUnit(params[expr.binder].toString())!!) + " "
                    }
                    else {
                        if (colorSettings.getColorOfUnit(it) != null)
                            colorSettings.literalWithColor(it, colorSettings.getColorOfUnit(it)!!) + " "
                        else
                            "$it "
                    }
                }
            var returnString = ""
            ifComponentList.forEach {
                returnString += if (ifComponentList.indexOf(it) == 1 && !expr.recursive)
                    ""
                else
                    it
            }
            returnString += "= "
            returnString += coloredExpr
            var inString = " in"
            val inColor = colorSettings.getColorOfUnit("let")
            if (inColor != null)
                inString = colorSettings.literalWithColor(inString, inColor)
            returnString += inString + "\n" + coloredBody
            returnString
        }
        is Expr.Lambda -> {
            val binderColor : Color?
            if(colorSettings.mode == colorSettings.modes.CASCADING) {
                params[expr.binder] = params.size.toString()
                binderColor = colorSettings.getColorOfUnit(params[expr.binder].toString())
            }
            else {
                params[expr.binder] = "binderLambda"
                binderColor = colorSettings.getColorOfUnit("binderLambda")
            }
            val coloredBody = toColorString(expr.body, params)
            var binderString = expr.binder

            if (binderColor != null)
                binderString = colorSettings.literalWithColor(binderString, binderColor)
            "\\$binderString => $coloredBody"
        }
        is Expr.Var -> {
            var varString = expr.name
            val varColor: Color? = if (params.contains(varString) && params[varString].equals("binderLambda"))
                colorSettings.getColorOfUnit("binderLambda")
            else if (params.contains(varString) && params[varString].equals("binderLet"))
                colorSettings.getColorOfUnit("binderLet")
            else if(params.contains(varString) && colorSettings.mode == colorSettings.modes.CASCADING)
                colorSettings.getColorOfUnit(params[expr.name].toString())
            else
                colorSettings.getColorOfUnit("var")
            if (varColor != null)
                varString = colorSettings.literalWithColor(varString, varColor)
            if (params.contains(varString))
                params.remove(varString)
            varString
        }
        is Expr.App -> {
            val coloredFuncBody = toColorString(expr.func, params)
            val coloredArg = toColorString(expr.arg, params)
            "$coloredFuncBody $coloredArg"
        }
    }
}


sealed class Value {
    data class Int(val num: kotlin.Int) : Value()
    data class Bool(val bool: Boolean) : Value()
    data class String(val string: kotlin.String) : Value()
    data class Closure(var env: Env, val binder: kotlin.String, val body: Expr) : Value()
}

enum class Operator {
    Add,
    Subtract,
    Multiply,
    Divide,
    Equality,
    Concat;

    fun toArithmeticSignString(): String {
        return when (this) {
            Add -> "+"
            Subtract -> "-"
            Multiply -> "*"
            Divide -> "/"
            Equality -> "=="
            Concat -> "#"
        }
    }
}

typealias Env = PersistentMap<String, Value>


fun eval(env: Env, expr: Expr): Value {
    return when (expr) {
        is Expr.IntLiteral -> Value.Int(expr.num)
        is Expr.BoolLiteral -> Value.Bool(expr.bool)
        is Expr.StringLiteral -> Value.String(expr.string)
        is Expr.Binary -> {
            val left = eval(env, expr.left)
            val right = eval(env, expr.right)
            return when (expr.op) {
                Operator.Equality -> if (left is Value.Int && right is Value.Int) {
                    Value.Bool(left.num == right.num)
                } else if (left is Value.Bool && right is Value.Bool) {
                    Value.Bool(left.bool == right.bool)
                } else if (left is Value.String && right is Value.String) {
                    Value.Bool(left.string == right.string)
                } else {
                    throw Error("Comparing incompatible values: $left and $right")
                }
                Operator.Concat -> if (left is Value.String && right is Value.String) {
                    Value.String(left.string + right.string)
                } else {
                    throw Error("Can't concatenate non-string values: $left and $right")
                }
                else -> numericBinary(left, right, nameForOp(expr.op)) { x, y -> applyOp(expr.op, x, y) }
            }

        }
        is Expr.If -> {
            val condition = eval(env, expr.condition)
            if (condition !is Value.Bool) {
                throw Exception("Expected a boolean condition, but got $condition")
            }
            return if (condition.bool) {
                eval(env, expr.thenBranch)
            } else {
                eval(env, expr.elseBranch)
            }
        }
        is Expr.Let -> {
            val evaledExpr = eval(env, expr.expr)
            if (expr.recursive && evaledExpr is Value.Closure) {
                evaledExpr.env = evaledExpr.env.put(expr.binder, evaledExpr)
            }
            val extendedEnv = env.put(expr.binder, evaledExpr)
            eval(extendedEnv, expr.body)
        }
        is Expr.Lambda -> Value.Closure(env, expr.binder, expr.body)
        is Expr.Var ->
            when (expr.name) {
                "#firstChar" -> {
                    val s = env["x"]!! as Value.String
                    Value.String(s.string.take(1))
                }
                "#remainingChars" -> {
                    val s = env["x"]!! as Value.String
                    Value.String(s.string.drop(1))
                }
                "#charCode" -> {
                    val s = env["x"]!! as Value.String
                    Value.Int(s.string[0].code)
                }
                "#codeChar" -> {
                    val x = env["x"]!! as Value.Int
                    Value.String(x.num.toChar().toString())
                }
                else -> env[expr.name] ?: throw Exception("Unbound variable ${expr.name}")
            }
        is Expr.App -> {
            val func = eval(env, expr.func)
            if (func !is Value.Closure) {
                throw Exception("$func is not a function")
            } else {
                val arg = eval(env, expr.arg)
                val newEnv = func.env.put(func.binder, arg)
                eval(newEnv, func.body)
            }
        }
    }
}

fun applyOp(op: Operator, x: Int, y: Int): Value {
    return when (op) {
        Operator.Add -> Value.Int(x + y)
        Operator.Subtract -> Value.Int(x - y)
        Operator.Multiply -> Value.Int(x * y)
        Operator.Divide -> Value.Int(x / y)
        Operator.Equality -> Value.Bool(x == y)
        else -> throw Error("Can't concat ints")
    }
}

fun nameForOp(op: Operator): String {
    return when (op) {
        Operator.Add -> "add"
        Operator.Subtract -> "subtract"
        Operator.Multiply -> "multiply"
        Operator.Divide -> "divide"
        Operator.Equality -> "compare"
        Operator.Concat -> "concat"
    }
}

fun numericBinary(left: Value, right: Value, operation: String, combine: (Int, Int) -> Value): Value {
    if (left is Value.Int && right is Value.Int) {
        return combine(left.num, right.num)
    } else {
        throw (Exception("Can't $operation non-numbers, $left, $right"))
    }
}

val emptyEnv: Env = persistentHashMapOf()