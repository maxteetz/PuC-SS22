class PeekableIterator<T>(private val iter: Iterator<T>) {
    private var lh: T? = null
    fun peek(): T? {
        lh = next()
        return lh
    }

    fun next(): T? {
        lh?.let { lh = null; return it }
        return if (iter.hasNext()) {
            iter.next()
        } else {
            null
        }
    }
}


class Lexer(input: String) {

    private val iter = PeekableIterator(input.iterator())
    private var lh: Token? = null

    fun next(): Token {
        chompWhitespace()
        lh?.let { it -> lh = null; return it }
        return when (val c = iter.next()) {
            null -> Token.EOF
            '(' -> Token.LPAREN
            ')' -> Token.RPAREN
            '\\' -> Token.BACKSLASH
            ':' -> Token.COLON
            '+' -> Token.PLUS
            '/' -> Token.DIVIDES
            '*' -> Token.MULTIPLY
            '#' -> Token.HASH
            '-' -> if (iter.peek() == '>') {
                iter.next()
                Token.ARROW
            } else {
                Token.MINUS
            }
            '=' -> if (iter.peek() == '>') {
                iter.next()
                Token.EQ_ARROW
            } else if (iter.peek() == '=') {
                iter.next()
                Token.DOUBLE_EQUALS
            } else {
                Token.EQUALS
            }
            '"' -> lexString()
            else -> when {
                c.isJavaIdentifierStart() -> lexIdentifier(c)
                c.isDigit() -> lexInt(c)
                else -> throw Exception("Unexpected $c")
            }
        }
    }

    private fun lexInt(first: Char): Token {
        var res = first.toString()
        while (iter.peek()?.isDigit() == true) {
            res += iter.next()
        }
        return Token.INT_LIT(res.toInt())
    }

    private fun lexIdentifier(first: Char): Token {
        var res = first.toString()
        while (iter.peek()?.isJavaIdentifierPart() == true) {
            res += iter.next()
        }
        return when (res) {
            "if" -> Token.IF
            "then" -> Token.THEN
            "else" -> Token.ELSE
            "let" -> Token.LET
            "rec" -> Token.REC
            "in" -> Token.IN
            "true" -> Token.BOOL_LIT(true)
            "false" -> Token.BOOL_LIT(false)
            "Int" -> Token.INT
            "Bool" -> Token.BOOL
            "String" -> Token.STRING
            else -> Token.IDENT(res)
        }
    }

    private fun lexString(): Token.STRING_LIT {
        var result = ""
        while (iter.peek() != '"') {
            val next = iter.next() ?: throw Error("Unterminated String Literal")
            result += next
        }
        iter.next()
        return Token.STRING_LIT(result)
    }

    private fun chompWhitespace() {
        while (iter.peek()?.isWhitespace() == true) {
            iter.next()
        }
    }

    fun lookahead(): Token {
        lh = next()
        return lh ?: Token.EOF
    }
}